// Content Loaded Event that creates the handlers for all calculator buttons
$(document).ready(function() {

	// Create JSON called data for calculator components
	var data = {
		"num1": "",
		"num2": "",
		"firstNum": true,
		"operation": "",
		"display": "0",
		"input": ""
	};


	// Ajax request the server to check for session data
	$.ajax({
		url: "Home/getSessionData",
		type: "POST",
		dataType: "json",
		data: JSON.stringify(data),
		success: function(response){
			data = response;
			// Log info to console to debug
			console.log(data);
			// Update display to show display string calculated in controller
			$("#calcText").text(data.display);
		}
	});


    // Number Event
	$(".buttonClicked").click(function(){

		// Set the input to the text of the clicked button
		data.input = $(this).text();
		// AJAX POST to php controller Home
		$.ajax({
			url: "Home/calculate",
			type: "POST",
			contentType: "json",
			data: JSON.stringify(data),
			success: function(response){
				data = response;
				// Log info to console to debug
				console.log(data);
				// Update display to show display string calculated in controller
				$("#calcText").text(data.display);
			}
		});

  	});

});