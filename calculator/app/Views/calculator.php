<!doctype html>

<html lang="en">
	<head>
  		<meta charset="utf-8">
	  	<title>Calculator</title>
	  	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	  	<link rel="stylesheet" type="text/css" href="mainStyle.css">
	</head>
	<body>
		<div class="col-3"></div>
		<div class="container col-6" id="calcBox">
			<div class="row">
				<div class="col-12" id="calcDisplay">
					<p id="calcText">0</p>
				</div>
			</div>
			<div class="row">
				<button class="col-3 buttonClicked" id="clear" type="button">C</button>
				<button class="col-3 singleNumOperation buttonClicked" type="button">+/-</button>
				<button class="col-3 singleNumOperation buttonClicked" type="button">%</button>
				<button class="col-3 twoNumOperation buttonClicked" type="button">/</button>
			</div>
			<div class="row">
				<button class="col-3 number buttonClicked">7</button>
				<button class="col-3 number buttonClicked" type="button">8</button>
				<button class="col-3 number buttonClicked" type="button">9</button>
				<button class="col-3 twoNumOperation buttonClicked" type="button">x</button>
			</div>
			<div class="row">
				<button class="col-3 number buttonClicked" type="button">4</button>
				<button class="col-3 number buttonClicked" type="button">5</button>
				<button class="col-3 number buttonClicked" type="button">6</button>
				<button class="col-3 twoNumOperation buttonClicked" type="button">-</button>
			</div>
			<div class="row">
				<button class="col-3 number buttonClicked" type="button">1</button>
				<button class="col-3 number buttonClicked" type="button">2</button>
				<button class="col-3 number buttonClicked" type="button">3</button>
				<button class="col-3 twoNumOperation buttonClicked" type="button">+</button>
			</div>
			<div class="row">
				<button class="col-6 number buttonClicked" type="button">0</button>
				<button class="col-3 number buttonClicked" type="button">.</button>
				<button class="col-3 buttonClicked" id="equals" type="button">=</button>
			</div>
		</div>
		<div class="col-3"></div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="calcButtonActions.js"></script>
	</body>
</html>
